from  node:8 as builder
workdir /usr/src/app
copy package*.json ./
run npm install
COPY src/ ./src/
COPY public/ ./public
RUN  npm run build

from  nginx:1.15.5
COPY --from=builder /usr/src/app/build/  /usr/share/nginx/html
EXPOSE 80